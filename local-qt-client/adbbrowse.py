#!/usr/bin/env python

import sys

from PySide.QtGui import *

from browser import BrowserView

if __name__ == '__main__':
    app = QApplication(sys.argv)

    browser = BrowserView()
    browser.show()

    app.exec_()