from PySide.QtGui import *
from PySide.QtWebKit import *


class JsExecutor(object):
    def __init__(self, web_frame):
        self.web_frame = web_frame

    def exec_js_code(self, name, *args):
        if self.web_frame:
            self.web_frame.evaluateJavaScript(name + '(' + ','.join(["'" + str(x) + "'" for x in args]) + ');')

    def __getattr__(self, item):
        def wrapper(*args):
            return self.exec_js_code(item, *args)

        return wrapper


class BrowserView(object):
    def __init__(self):
        self.document = None
        self.frame = None
        self.search = None
        self.js = None

        self.window = QWidget()
        self.window.resize(1200, 600)

        vbox = QVBoxLayout()
        vbox.setContentsMargins(2, 2, 2, 2)

        self.toolbar = self.setup_toolbar()

        self.web = QWebView()
        self.web.settings()
        self.web.loadFinished.connect(self.load_finished)
        self.web.load('holder.html')

        vbox.addWidget(self.toolbar)
        vbox.addWidget(self.web)
        self.window.setLayout(vbox)

        self.web.show()

    def load_finished(self):
        self.frame = self.web.page().mainFrame()
        self.js = JsExecutor(self.frame)
        self.document = self.web.page().mainFrame().findFirstElement('div.end')

        self.write_line('error', 'This is error message')
        self.write_line('warning', 'This is warning message')
        self.write_line('info', 'This is info message')
        self.write_line('debug', 'This is debug message')

    def execute(self, js):
        if self.frame:
            self.frame.evaluateJavaScript(js)

    def write_line(self, style, content):
        if self.document:
            self.document.appendInside('<div class="%s log">%s</div>' % (style, content))

    def show(self):
        self.window.show()

    def setup_toolbar(self):

        toolbar = QWidget()
        hbox = QHBoxLayout()
        hbox.addWidget(QLabel("Search"))
        self.search = QLineEdit()
        self.search.textEdited.connect(self.on_search_increment)
        hbox.addWidget(self.search)
        hbox.addStretch()

        toolbar.setLayout(hbox)
        return toolbar

    def on_search_increment(self):
        text = self.search.text()
        if text:
            self.js.searchAndHighlight(text)
            #self.execute("searchAndHighlight('%s')" % text)
        else:
            self.js.removeHighlight()
            #self.execute("removeHighlight()")
