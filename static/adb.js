var autoScroll = true;

var styleMatcher = {
    'D': "debug",
    'E': "error",
    'W': "warning",
    'I': "info",
    'V': "verbose",
    'A': "assert",
    'F': "wtf"};

var filterMatcher = {
    'level': 2,
    'tag': 3,
    'text': 4
};

var splitter = /^(\[.*\]) ([\w])\/(.*)\([^\d]*(\d+).*\): (.*)$/im;

var filters = {};

String.prototype.limit = function (len) {
    var result = this;
    if (this.length > len) {
        result = this.substring(0, len - 1);
        var i = result.length / 2;
        var before = result.substr(0, i);
        var after = result.substr(i);
        result = before + "\u2026" + after;
    }
    return result;
};

function scrollDown() {
    if (autoScroll) {
        $(window).scrollTop($(document).height() - $(window).innerHeight());
    }
}

function error(message) {
    return '<div class="server-error">' + message + '</div>';
}
function processLog(data) {
    var jsonData = JSON.parse(data);
    if ('log' in jsonData) {
        return formatLogLine(jsonData['log']);
    } else if ('failure' in jsonData) {
        return error(jsonData['failure']);
    } else {
        return null;
    }
}

function hasPassedFilters(line) {
    function matches(line, filter_name) {
        if (!filter_name in filters) {
            return true;
        }
        var matchingFilterCollection = filters[filter_name];
        console.log(filters, matchingFilterCollection);
        for (var filter in matchingFilterCollection) {
            var index = 0;
            if (filter in filterMatcher) {
                index = filterMatcher[filter];
            }
            var matchers_list = matchingFilterCollection[filter];
            var matched = false;
            matchers_list.forEach(function (re) {
                matched = matched || (line[index].match(new RegExp(re)) != null); // todo: optimize this
            })
            if (matched) {
                return true;
            }
        }
        return false;
    }

    var matchedInclude = filters.include ? matches(line, 'include') : true;
    var matchedExclude = filters.exclude ? matches(line, 'exclude') : false;

    return matchedInclude && !matchedExclude;
}

function formatLogLine(line) {
    var results = splitter.exec(line.trim());
    if (results != null && results.length > 1) {
        var className = "log " + styleMatcher[results[2]];
        if (hasPassedFilters(results)) {
            return '<div class="' + className + '">' + results[1] + " " + results[2] + " <div class='tag'>" +
                results[3].trim().limit(16) + "</div><div class='pid'>" + results[4].trim() + "</div> " + results[5] + "</div>";
        } else {
            return null;
        }
    }
    return '<div class="log">' + line + '</div>';
}

function setAutoScroll(value) {
    autoScroll = value;
    $('#autoscroll').prop('checked', value);
}

function onScroll() {
    if (autoScroll) {
        setAutoScroll(false);
    }
}

function filterToString(filter) {
    var result = "";
    if (filter == undefined) {
        return result;
    }
    for (var x in filterMatcher) {
        for (var i in filter[x]) {
            result += x + ':' + filter[x][i] + '\n';
        }
    }
    return result;
}

function stringToFilter(filterString) {
    var items = filterString.split('\n');
    var result = {};
    items.forEach(function (item) {
        if (item.length > 0) {
            var sepIndex = item.indexOf(':');
            var name = item.substring(0, sepIndex).trim();
            var value = item.substring(sepIndex + 1).trim();
            if (name != '' && name in result) {
                result[name].push(value);
            } else {
                result[name] = [value];
            }
        }
    });
    return result;
}

function supports_html5_storage() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}

function checkCapabilities() {
    return supports_html5_storage();
}

function init() {
    if (checkCapabilities()) {

        if (window.addEventListener) {
            document.addEventListener('DOMMouseScroll', function (event) {
                if (event.detail < 0) {
                    onScroll();
                }
            }, false);
        }

        document.onmousewheel = function (event) {
            if (event.wheelDelta > 0) {
                onScroll();
            }
        };

        document.onkeydown = function (e) {
            var event = window.event ? window.event : e;
            if (event.keyCode == 35) {
                setAutoScroll(true);
            }
        };

        $('#autoscroll').change(function () {
            autoScroll = $(this).is(':checked');
        });

        $('#clear_btn').click(function () {
            $('div.log').remove();
        });

        $('#edit_filters').click(function () {
            $('#include').text(filterToString(filters.include));
            $('#exclude').text(filterToString(filters.exclude));
            $('#filters').show(250);
        });

        $('#save_filters').click(function () {
            filters.include = stringToFilter($('#include').val());
            filters.exclude = stringToFilter($('#exclude').val());
            $('#filters').hide(250);
            updateFilterStorage();
        });

        $('#close_filters').click(function () {
            $('#filters').hide(250);
        });

        load_initial_filters();

        request_logs()
    } else {
        $('#content').append(error('Your browser is not compatible.'));
    }
}

function load_initial_filters() {
    if (localStorage['filters'] == null) {
        $.get('/get_filter', {}, function (data) {
            if (data != null) {
                filters = data;
            }
        });
        updateFilterStorage();
    } else {
        filters = JSON.parse(localStorage['filters'])
    }
}

function updateFilterStorage() {
    localStorage['filters'] = JSON.stringify(filters);
}

// Main function which establishes WebSocket connection to server and waits for ADB log
function request_logs() {
    var ws = new WebSocket("ws://localhost:18081/websocket");
    ws.onopen = function () {
        ws.send("ready");
    };
    ws.onmessage = function (evt) {
        var elem = processLog(evt.data);
        if (elem != null) {
            $("#content").append(elem);
            scrollDown();
        }
    }
}
