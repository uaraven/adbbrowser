import os
import platform

from datetime import datetime
import gevent.subprocess as subprocess

SDK_HOME = 'ANDROID_SDK_HOME'

DEFAULT_ENCODING = 'cp1252'
DT_FORMAT = '%Y-%m-%d %H:%M:%S'


class AdbGenerator(object):
    def __init__(self):
        self.terminated = False

        if platform.system() == 'Windows':
            self.adb_exec = 'adb.exe'
        else:
            self.adb_exec = 'adb'

        if SDK_HOME in os.environ:
            self.adb_path = os.path.join(os.environ[SDK_HOME], 'platform-tools', self.adb_exec)
        else:
            self.adb_path = None

    def isLogAvailable(self):
        """Checks availability of device through adb"""
        if self.adb_path:
            adb = subprocess.Popen([self.adb_path, 'devices'], stdout=subprocess.PIPE, shell=True)
        else:
            adb = subprocess.Popen([self.adb_exec, 'devices'], stdout=subprocess.PIPE)

        output = adb.stdout.readlines()
        try:
            for st in output:
                if st.find('\tdevice') > 0:
                    return True
        finally:
            adb.wait()

        return False

    def start(self):
        """Starts 'adb logcat'"""
        if self.adb_path:
            self.adb = subprocess.Popen([self.adb_path, 'logcat'], stdout=subprocess.PIPE, shell=True)
        else:
            self.adb = subprocess.Popen([self.adb_exec, 'logcat'], stdout=subprocess.PIPE)

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        if self.adb.poll() is not None or self.terminated:
            self.terminate()
            raise StopIteration
        line = unicode(self.adb.stdout.readline(), DEFAULT_ENCODING)
        line = self.post_process(line)
        return line

    def terminate(self):
        try:
            if self.adb.poll() is not None:
                self.adb.terminate()
        except Exception as ex:
            print 'Failed to terminate adb:', ex

    @staticmethod
    def post_process(line):
        return '[' + datetime.now().strftime(DT_FORMAT) + '] ' + line


def logcat():
    return AdbGenerator()
