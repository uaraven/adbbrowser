gevent 1.0 (beta)
=================

Version 1.0 is required, because it contains greenlet-friendly subprocess module

To install:

sudo pip-2.7 install cython git+git://github.com/surfly/gevent.git@1.0rc3#egg=gevent

Following may be required:
  git clone git://github.com/surfly/gevent.git
  cd gevent
  python setup.py install

or just install egg file (for x64)

greenlet 0.4.1
==============

Should be installed automatically by gevent

gevent-websocket
================

easy_install gevent-websocket


libevent & libevent-dev
=======================

Use your distro package management.


Bottle
======
easy_install bottle

