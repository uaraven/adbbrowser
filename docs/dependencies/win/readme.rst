Installing on Windows
=====================

You will need 32-bit Python.

If you still don't have setuptools run python ez_setup.py

Install cython 0.19.1 or higher, then install gevent 1.0 (rc2 or higher).
Install gevent-websockets using easyinstall
Install bottle using easyinstall

You're set up.

