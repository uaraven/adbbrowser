Main features:

* Fail if there is no devices connected to adb (send different template, asking to refresh)
* Send initial filters (stored in file) to browser (or use ajax request) - done
* Edit filters in browser - done
** Store edited filters to file (definitely ajax) - done
