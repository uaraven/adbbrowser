def read_filters():
    with open('filter.rules') as inp:
        lines = inp.readlines()
        return '\n'.join(lines)
