import os
import json

from bottle import Bottle, request, static_file, abort, response
from geventwebsocket import WebSocketHandler
from gevent.pywsgi import WSGIServer
import filter

app = Bottle()

__html_path = os.path.join(os.path.dirname(__file__), 'html')
__static_path = os.path.join(os.path.dirname(__file__), 'static')
__streamer = None


def serve_logs_to_client(websock):
    __streamer.start()
    while True:
        try:
            for x in __streamer:
                websock.send(json.dumps({'log': x}))
            websock.send(StopIteration)
        except Exception as ex:
            print 'Connection terminated (%s)' % ex
            break
    __streamer.terminate()


def error(message):
    return json.dumps({'failure': message})


@app.route("/static/<file_name:path>")
def static(file_name):
    return static_file(file_name, __static_path)


@app.route("/")
def index():
    return static_file('ws.html', root=__html_path)


@app.route("/websocket")
def websocket_handler():
    web_socket = request.environ.get('wsgi.websocket')
    if web_socket:
        if __streamer.isLogAvailable():
            serve_logs_to_client(web_socket)
        else:
            web_socket.send(error('No devices connected or adb not found. Check device connection and refresh page.'))
    else:
        abort(500, 'WebSocket request expected by server')


@app.route("/get_filter")
def get_filter_handler():
    response.content_type = 'application/json'
    filters = filter.read_filters()
    print 'Filters:', filters
    return filters


def start_serving(streamer):
    global __streamer
    __streamer = streamer
    server = WSGIServer(("0.0.0.0", 18081), app, handler_class=WebSocketHandler)
    server.serve_forever()
